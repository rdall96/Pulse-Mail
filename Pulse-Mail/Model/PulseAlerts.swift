//
//  PulseAlerts.swift
//  Pulse Mail
//
//  Created by Ricky Dall'Armellina on 12/9/18.
//  Copyright © 2018 Ricky Dall'Armellina. All rights reserved.
//

import Foundation
import AppKit

// Class to create and manage custom alert pop-ups for the email service

class PulseAlerts {
    
    public static func createWarningAlert(title: String, info: String) {
        let alert = NSAlert()
        alert.messageText = title
        alert.informativeText = info
        alert.alertStyle = .warning
        alert.addButton(withTitle: "OK. Got it.")
        alert.runModal()
    }
    
    public static func createErrorAlert(title: String, info: String) {
        let alert = NSAlert()
        alert.messageText = title
        alert.informativeText = info
        alert.alertStyle = .critical
        alert.addButton(withTitle: "Oh, okay.")
        alert.runModal()
    }
    
    public static func createInfoAlert(title: String, info: String) {
        let alert = NSAlert()
        alert.messageText = title
        alert.informativeText = info
        alert.alertStyle = .informational
        alert.addButton(withTitle: "Okay, thanks!")
        alert.runModal()
    }
    
}
