//
//  Email Manager.swift
//  Pulse Mail
//
//  Created by Ricky Dall'Armellina on 11/27/18.
//

import Foundation
import SwiftSMTP
import AppKit

class EmailManager {

    
    // MARK: Data
    private var smtp: SMTP!
    private var senderUser: Mail.User!
    private var senderAddress: String
    private var password: String
    private var serverHostname: String
    
    
    // MARK: Initializers
    
    public init() {
        
        self.senderAddress = ""
        self.password = ""
        self.serverHostname = ""
        
    }
    
    public init(email: String, password: String, server: String) {
        
        self.senderAddress = email
        self.password = password
        self.serverHostname = server
        
    }
    
    
    // MARK: Data functions
    
    public var getSenderAddress: String { return self.senderAddress }
    
    public var getPassword: String { return self.password }
    
    public var getServerHostname: String { return self.serverHostname }
    
    public func setSenderAddress(address: String) { self.senderAddress = address }
    
    public func setPassword(password: String) { self.password = password }
    
    public func setServerHostname(server: String) { self.serverHostname = server }
    
    
    // MARK: Sender Functions
    
    // returns true if emails were attempted to be sent, false otherwise
    public func send(name: String, address: String, subject: String, message: String) {
        
        //setup sender
        smtp = SMTP(hostname: serverHostname, email: self.senderAddress, password: self.password)
        senderUser = Mail.User(email: self.senderAddress)
        // setup recepient
        let destUser = Mail.User(name: name, email: address)
        // setup email
        let mail = Mail(from: senderUser, to: [destUser], subject: subject, text: message)
        // send email
        smtp.send(mail)
        
    }
    
    public func sendWithAttachment(name: String, address: String, subject: String, message: String, attach: String) {
        
        //setup sender
        smtp = SMTP(hostname: serverHostname, email: self.senderAddress, password: self.password)
        senderUser = Mail.User(email: self.senderAddress)
        // setup recepient
        let destUser = Mail.User(name: name, email: address)
        // setup attachment
        let fileAttachemnt = Attachment(filePath: attach)
        // setup email
        let mail = Mail(from: senderUser, to: [destUser], subject: subject, text: message, attachments: [fileAttachemnt])
        // send email
        smtp.send(mail)
    }

    // Checks if sender info is valid or has not been input, returns false if ready to send email
    public func checkValidSenderInfo() -> Bool {
        
        if senderAddress == "" || password == "" || serverHostname == "" {
            if senderAddress == "" {
                PulseAlerts.createWarningAlert(title: "Wait!", info: "Please insert a valid email address in the preferences to send emails with.")
                return false
            }
            if password == "" {
                PulseAlerts.createWarningAlert(title: "Wait!", info: "Please insert a valid email password in the preferences.")
                return false
            }
            if serverHostname == "" {
                PulseAlerts.createWarningAlert(title: "Wait!", info: "Please insert a valid server in the preferences.")
                return false
            }
        }
        else {
            return true
        }
        return false
    }
    
    public static func getAttachmentPaths(folder_path: String, file_name: String) -> String { return (folder_path + file_name) }

    
}
