//
//  CSVParser.swift
//  Pulse Mail
//
//  Created by Ricky Dall'Armellina on 11/28/18.
//

import Foundation
import CSV

class CSVParser {
    
    
    // MARK: Data
    private var people: [(email: String, name: String, fileID: String)] = [] // array of tuples format: (email, name, imgID)
    private var addressDB: CSVReader!
    private var hasAttachment: Bool

    
    // MARK: Initializer
    
    public init(file_path: String, has_attachments: Bool) {
        
        self.hasAttachment = has_attachments
        
        // get the file address, open it as a CSV object (include header row)
        let inputFileData = try! NSString(contentsOfFile: file_path, encoding: String.Encoding.ascii.rawValue)
        
        parseAddresList(file: inputFileData)
        
    }
    
    
    //MARK: Data functions
    
    public var peopleList: [(email: String, name: String, fileID: String)] { return people }
    
    public var emailList: [String] {
        var emails: [String] = []
        for item in people {
            emails.append(item.email)
        }
        return emails
    }
    
    public var namesList: [String] {
        var names: [String] = []
        for item in people {
            names.append(item.name)
        }
        return names
    }
    
    public var attachmentList: [String] {
        var files: [String] = []
        for item in people {
            files.append(item.fileID)
        }
        return files
    }
    
    public var hasAttachments: Bool { return hasAttachment }
    
    public var getPeopleCount: Int { return people.count }
    
    
    // MARK: File parsing
    
    private func parseAddresList(file: NSString) {
        // parse the file and populate the people array
        
        // get the names from the property list
        let csvSettings = (name: SwiftyPlistManager.shared.fetchValue(for: "csv-first_name", fromPlistWithName: "settings") as! String, email: SwiftyPlistManager.shared.fetchValue(for: "csv-email", fromPlistWithName: "settings") as! String, fileID: SwiftyPlistManager.shared.fetchValue(for: "csv-attachment", fromPlistWithName: "settings") as! String)
        
        // for some odd reason there 3 characters at the beginning og the file, so skip them
        let inputFileData = file.substring(from: 3) as NSString
        try! addressDB = CSVReader(string: inputFileData as String, hasHeaderRow: true)
        
        
        // get the values and add them to the people array
        if self.hasAttachment {
            while addressDB.next() != nil {
                people.append((email: addressDB[csvSettings.email]!, name: addressDB[csvSettings.name]!, fileID: addressDB[csvSettings.fileID]!))
            }
        }
        else {
            while addressDB.next() != nil {
                people.append((email: addressDB[csvSettings.email]!, name: addressDB[csvSettings.name]!, fileID: ""))
            }
        }
        
    }


}
