//
//  PreferencesViewController.swift
//  Pulse-Mail
//
//  Created by Ricky Dall'Armellina on 11/29/18.
//  Copyright © 2018 Ricky Dall'Armellina. All rights reserved.
//

import Cocoa

class PreferencesViewController: NSViewController {

    // MARK: Preperty Outlets
    @IBOutlet weak var senderEmailAddressField: NSTextField!
    @IBOutlet weak var senderPasswordField: NSSecureTextField!
    @IBOutlet weak var senderServerMenuPicker: NSPopUpButton!
    @IBOutlet weak var senderOtherServerField: NSTextField!
    @IBOutlet weak var csvFirstNameField: NSTextField!
    @IBOutlet weak var csvLastNameField: NSTextField!
    @IBOutlet weak var csvEmailField: NSTextField!
    @IBOutlet weak var csvAttachmentField: NSTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Update fields when preferences are re-opened
        senderEmailAddressField.stringValue = SwiftyPlistManager.shared.fetchValue(for: "email-address", fromPlistWithName: "settings") as! String
        senderPasswordField.stringValue = SwiftyPlistManager.shared.fetchValue(for: "email-password", fromPlistWithName: "settings") as! String
        senderServerMenuPicker.selectedItem?.title = SwiftyPlistManager.shared.fetchValue(for: "email-server", fromPlistWithName: "settings") as! String
        csvFirstNameField.stringValue = SwiftyPlistManager.shared.fetchValue(for: "csv-first_name", fromPlistWithName: "settings") as! String
        csvLastNameField.stringValue = SwiftyPlistManager.shared.fetchValue(for: "csv-last_name", fromPlistWithName: "settings") as! String
        csvEmailField.stringValue = SwiftyPlistManager.shared.fetchValue(for: "csv-email", fromPlistWithName: "settings") as! String
        csvAttachmentField.stringValue = SwiftyPlistManager.shared.fetchValue(for: "csv-attachment", fromPlistWithName: "settings") as! String
    }
    
    
    // MARK: Actions
    
    @IBAction func senderEmailFieldChanged(_ sender: NSTextField) {
        // set new email address setting in settings.plist
        SwiftyPlistManager.shared.save(senderEmailAddressField.stringValue, forKey: "email-address", toPlistWithName: "settings")
    }

    @IBAction func senderPasswordFieldChanged(_ sender: NSSecureTextField) {
        // se new passowrd setting in settings.plist
        SwiftyPlistManager.shared.save(senderPasswordField.stringValue, forKey: "email-password", toPlistWithName: "settings")
    }
    
    @IBAction func senderServerMenuItemChanged(_ sender: NSPopUpButton) {
        // set new server setting in settings.plist
        
        // if item = other then activate senderOtherServerField
        if senderServerMenuPicker.selectedItem?.title == "Other..." {
            senderOtherServerField.isEditable = true
        }
        else { senderOtherServerField.isEditable = false }
        
        // send selected server to smtp
        SwiftyPlistManager.shared.save((senderServerMenuPicker.selectedItem?.title)!, forKey: "email-server", toPlistWithName: "settings")
        
    }
    
    // FIXME: Other server option not working
    @IBAction func senderOtherServerFieldChanged(_ sender: NSTextField) {
        // if anything typed in this field then set the menu picker to "Other..."
        //senderServerMenuPicker.selectItem(withTitle: "Other...")
    }
    
    @IBAction func csvFirstNameFieldChanged(_ sender: NSTextField) {
        SwiftyPlistManager.shared.save(csvFirstNameField.stringValue, forKey: "csv-first_name", toPlistWithName: "settings")
    }
    
    @IBAction func csvLastNameFieldChanged(_ sender: NSTextField) {
        SwiftyPlistManager.shared.save(csvLastNameField.stringValue, forKey: "csv-last_name", toPlistWithName: "settings")
    }
    
    @IBAction func csvEmailFieldChanged(_ sender: NSTextField) {
        SwiftyPlistManager.shared.save(csvEmailField.stringValue, forKey: "csv-email", toPlistWithName: "settings")
    }
    
    @IBAction func csvAttachmentFieldChanged(_ sender: NSTextField) {
        SwiftyPlistManager.shared.save(csvAttachmentField.stringValue, forKey: "csv-attachment", toPlistWithName: "settings")
    }
    
    
}
