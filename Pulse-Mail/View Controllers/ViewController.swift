//
//  ViewController.swift
//  Pulse-Mail
//
//  Created by Ricky Dall'Armellina on 11/28/18.
//  Copyright © 2018 Ricky Dall'Armellina. All rights reserved.
//

import Cocoa


class ViewController: NSViewController {
    
    
    // MARK: Property Outlets
    @IBOutlet weak var sendEmailBtn: NSButton!
    @IBOutlet weak var clearFieldsBtn: NSButton!
    @IBOutlet weak var emailRecipientsListField: NSTextField!
    @IBOutlet weak var emailSubjectField: NSTextField!
    @IBOutlet weak var emailMessageField: NSTextView!
    @IBOutlet weak var emailAttachmentsFolderField: NSTextField!
    @IBOutlet weak var addressListBrowseBtn: NSButton!
    @IBOutlet weak var attachmentFolderBrowseBtn: NSButton!
    @IBOutlet weak var emailSendingProgress: NSProgressIndicator!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    // MARK: Actions

    @IBAction func sendEmailBtnClicked(_ sender: NSButton) {
        
        // Create email object
        let email: EmailManager = EmailManager(email: (SwiftyPlistManager.shared.fetchValue(for: "email-address", fromPlistWithName: "settings") as! String), password: (SwiftyPlistManager.shared.fetchValue(for: "email-password", fromPlistWithName: "settings") as! String), server: (SwiftyPlistManager.shared.fetchValue(for: "email-server", fromPlistWithName: "settings") as! String))
        
        // Check is address field is empty first
        if emailRecipientsListField.stringValue == "" {
            PulseAlerts.createWarningAlert(title: "Missing information!", info: "Please, provide a CSV file for the mailing list.")
        }
        else {
            
            // Detect correct kind of address list CSV
            let addressList: CSVParser
            // Populate addressList with the data
            if emailAttachmentsFolderField.stringValue != "" {
                addressList = CSVParser(file_path: emailRecipientsListField.stringValue, has_attachments: true)
            }
            else {
                addressList = CSVParser(file_path: emailRecipientsListField.stringValue, has_attachments: false)
            }
            
            // check if sender info is valid before attempting to send email
            if email.checkValidSenderInfo() {
                
                // Setup status bar
                //emailSendingProgress.minValue = 0
                emailSendingProgress.maxValue = Double(addressList.getPeopleCount)
                //emailSendingProgress.doubleValue = 0 // set position at 0
                emailSendingProgress.usesThreadedAnimation = true // make the animation run on a separate thread so it's always visible
                //emailSendingProgress.isHidden = false // make visible
                
                // get every person in the list and send an email with that data
                if addressList.hasAttachments {
                    for person in addressList.peopleList {
                        let attachment = EmailManager.getAttachmentPaths(folder_path: emailAttachmentsFolderField.stringValue, file_name: person.fileID)
                        // update status bar
                        self.emailSendingProgress.increment(by: 1.0)
                        email.sendWithAttachment(name: person.name, address: person.email, subject: emailSubjectField.stringValue, message: emailMessageField.string, attach: attachment)
                    }
                }
                else {
                    for person in addressList.peopleList {
                        // update status bar
                        self.emailSendingProgress.increment(by: 1.0)
                        email.send(name: person.name, address: person.email, subject: emailSubjectField.stringValue, message: emailMessageField.string)
                    }
                }
            }
        }
        
        // Reset status bar
        emailSendingProgress.doubleValue = 0.0 // reset at zero
        //emailSendingProgress.isHidden = true // hide it again

    }

    @IBAction func clearFieldsBtnClicked(_ sender: NSButton) {
        // clear all text fields
        emailRecipientsListField.stringValue = ""
        emailSubjectField.stringValue = ""
        emailMessageField.string = ""
        emailAttachmentsFolderField.stringValue = ""
    }

    @IBAction func addressListBrowseBtnClicked(_ sender: NSButton) {
        
        // NOTE: code from https://denbeke.be/blog/programming/swift-open-file-dialog-with-nsopenpanel/
        
        let dialog = NSOpenPanel();
        
        dialog.title                   = "Choose an addresses CSV file";
        dialog.showsResizeIndicator    = true;
        dialog.showsHiddenFiles        = false;
        dialog.canChooseDirectories    = true;
        dialog.canCreateDirectories    = true;
        dialog.allowsMultipleSelection = false;
        dialog.allowedFileTypes        = ["csv"];
        
        if (dialog.runModal() == NSApplication.ModalResponse.OK) {
            let result = dialog.url // Pathname of the file
            
            if (result != nil) {
                let path = result!.path
                emailRecipientsListField.stringValue = path
            }
        } else {
            // User clicked on "Cancel"
            return
        }
        
    }
    
    @IBAction func attachmentFolderBrowseBtnClicked(_ sender: NSButton) {
        
        // NOTE: code from https://denbeke.be/blog/programming/swift-open-file-dialog-with-nsopenpanel/
        
        let dialog = NSOpenPanel();
        
        dialog.title                   = "Choose a folder of files to attach";
        dialog.showsResizeIndicator    = true;
        dialog.showsHiddenFiles        = false;
        dialog.canChooseDirectories    = true;
        dialog.canCreateDirectories    = true;
        dialog.allowsMultipleSelection = false;
        dialog.allowedFileTypes        = [];
        
        if (dialog.runModal() == NSApplication.ModalResponse.OK) {
            let result = dialog.url // Pathname of the file
            
            if (result != nil) {
                let path = result!.path
                emailAttachmentsFolderField.stringValue = (path + "/")
            }
        } else {
            // User clicked on "Cancel"
            return
        }
        
    }
    
    
}
