//
//  AppDelegate.swift
//  Pulse-Mail
//
//  Created by Ricky Dall'Armellina on 11/28/18.
//  Copyright © 2018 Ricky Dall'Armellina. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        
        // Start plist manager in shared mode
        SwiftyPlistManager.shared.start(plistNames: ["settings"], logging: false)
        
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

